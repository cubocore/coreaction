/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QTimer>
#include <QFile>
#include <QDir>
#include <QColor>
#include <QByteArray>
#include <QString>
#include <QGridLayout>
#include <QTimerEvent>

#include <csys/infomanager.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

#include "wsystem.h"


static QByteArray getSvg( double mFraction, QString name )
{
	int green = 0, red = 0;
	// Change from green to Yellow: When mFraction = 0, red = 0; mFraction = 0.4, red = 255
	if ( mFraction <= 0.4 ) {
        green = static_cast<int>( 255 );
        red = static_cast<int>( mFraction * 638 );
	}

	// Remain Yellow
	else if ( mFraction <= 0.6 ) {
		green = 255;
		red = 255;
	}

	// Change from Yellow to red: When totalF = 0.6, green = 255; mFraction = 1, green = 0;
	else {
        green = static_cast<int>( ( 1 - mFraction ) * 638 );
        red = static_cast<int>( 255 );
	}

	QColor strokeColor = QPalette().color( QPalette::Highlight );

	QFile svg( name );
	svg.open( QFile::ReadOnly );
	QByteArray contents = svg.readAll().replace( "#ffffff", QColor( red, green, 0 ).name().toLatin1().data() );
	contents = contents.replace( "#XXXXXX", strokeColor.name().toLatin1().data() );
	svg.close();

	return contents;
}

wSystem::wSystem( QWidget *parent ) : QWidget( parent ),
	info( new InfoManager )
{
	info->setDataCount(240);
	info->setUpdateInterval(500);
	info->update();

	/** InfoManager update timer */
	QTimer *timer = new QTimer(this);
	timer->setTimerType( Qt::PreciseTimer );    // Precise Timer: update every 500 ms sharp
	timer->setInterval( 500 );                 // We needs points every 500 ms.
	timer->setSingleShot( false );              // This timer keeps repeating.

	timer->callOnTimeout(
		[=]() {
			info->update();
		}
	);

	timer->start();

    cpuSvg = new QSvgWidget( this );
    cpuSvg->setFixedSize( QSize(24,24) );

    ramSvg = new QSvgWidget( this );
    ramSvg->setFixedSize( QSize(24,24) );

    rootSvg = new QSvgWidget( this );
    rootSvg->setFixedSize( QSize(24,24) );

    homeSvg = new QSvgWidget( this );
    homeSvg->setFixedSize(QSize(24,24) );

    cpuLbl = new QLabel( this );
    ramLbl = new QLabel( this );
    rootLbl = new QLabel( this );
    homeLbl = new QLabel( this );

    struct stat root;
    struct stat home;
    stat( "/", &root );
    stat( QDir::homePath().toLocal8Bit().data(), &home );

    QLabel *lbl = new QLabel("SYSTEM");
    lbl->setFont(QFont(font().family(), 11));
    lbl->setAlignment(Qt::AlignLeft);

    QGridLayout *lyt = new QGridLayout();
    lyt->setSpacing(2);
	lyt->setContentsMargins(0, 0, 0, 0);
    QVBoxLayout *lytM = new QVBoxLayout();

    lyt->addWidget( cpuSvg, 0, 0, Qt::AlignCenter );
    lyt->addWidget( cpuLbl, 1, 0, Qt::AlignCenter );
    lyt->addWidget( ramSvg, 0, 1, Qt::AlignCenter );
    lyt->addWidget( ramLbl, 1, 1, Qt::AlignCenter );
    lyt->addWidget( rootSvg, 0, 2, Qt::AlignCenter );
    lyt->addWidget( rootLbl, 1, 2, Qt::AlignCenter );

    if ( root.st_dev != home.st_dev ) {
        lyt->addWidget( homeSvg, 0, 3, Qt::AlignCenter );
        lyt->addWidget( homeLbl, 1, 3, Qt::AlignCenter );
    } else {
        homeSvg->setVisible(false);
        homeLbl->setVisible(false);
    }

    lytM->addWidget(lbl);
    lytM->addItem(lyt);

    setLayout( lytM );

    loadInfo();

    updateTimer.start( 1000, this );
}

wSystem::~wSystem() {
	delete info;
}

void wSystem::loadInfo()
{
    //set cpu value
    double cpuPercent = 0;
	int cpuCount = info->getCpuCoreCount();

	QList<QList<double>> cpuPercents( info->getCpuPercents(1) );
	Q_FOREACH( auto pc, cpuPercents ) {
		cpuPercent += pc[0];
	}

	cpuPercent = cpuPercent / (1.0 * cpuCount);
    cpuSvg->load( getSvg( cpuPercent / 100.0, ":/cpu.svg" ) );
    cpuLbl->setText( QString( "CPU: %1%" ).arg( static_cast<int>( cpuPercent )) );

    // set ram bar value
    double memPercent = 0;
	QList<double> memUsage( info->getMemUsed(1) );
	if (info->getMemTotal()) {
		memPercent = (memUsage[ 0 ] / (1.0 * info->getMemTotal())) * 100.0;
	}

	ramSvg->load( getSvg( memPercent / 100.0, ":/ram.svg" ) );
	ramLbl->setText( QString( "RAM: %1%" ).arg( static_cast<int>( memPercent ) ) );

	struct statvfs vfs;
	statvfs( "/", &vfs );
	double rootUse = 1.0 - 1.0 * vfs.f_bavail / vfs.f_blocks;
    rootSvg->load( getSvg( rootUse, ":/hdd.svg" ) );
    rootLbl->setText( QString( "Root: %1%" ).arg( static_cast<int>( rootUse * 100.0 ) ) );

	statvfs( QDir::homePath().toLocal8Bit().data(), &vfs );
	double homeUse = 1.0 - 1.0 * vfs.f_bavail / vfs.f_blocks;
    homeSvg->load( getSvg( homeUse, ":/hdd.svg" ) );
    homeLbl->setText( QString( "Home: %1%" ).arg( static_cast<int>( homeUse * 100.0 ) ) );
}

void wSystem::timerEvent( QTimerEvent *tEvent )
{
	if ( updateTimer.timerId() == tEvent->timerId() ) {
		loadInfo();
	}
}

/* Name of the plugin */
QString systemplugin::name()
{
    return "System";
}

/* The plugin version */
QString systemplugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *systemplugin::widget(QWidget *parent)
{
    return new wSystem(parent);
}
