/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QSystemTrayIcon>
#include <QBasicTimer>

#include "settings.h"

class QMenu;
class InfoManager;

namespace Ui {
    class coreaction;
}

class coreaction : public QWidget {
    Q_OBJECT

public:
    explicit coreaction(QWidget *parent = nullptr);
    ~coreaction();

public slots:
    void ShowWindow(QSystemTrayIcon::ActivationReason Reason);

private slots :
	void updateTime();

protected:
	void changeEvent( QEvent *cEvent ) override;
	void timerEvent(QTimerEvent *event) override;

private:
    Ui::coreaction *ui;
    settings *smi;
	QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    InfoManager *im;
	QBasicTimer timer;
    int uiMode;
    QStringList selectedPlugins;

    void widget();
    void trayicon();
    void addPlugins();
    void loadSettings();
};
